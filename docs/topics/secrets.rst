=======
Secrets
=======

Idem secrets management leverages the Vault secrets management system.

Connect to Vault
----------------

You can store credentials to connect to Vault in the encrypted credentials.yaml file used for Idem authentication. To connect, the credential profile must include at least the address and token.

.. code-block::

    vault:
      idem_vault:
        address: https://vault.example.com:8200/
        token: alphanumeric_token
        version: v2

Reference an existing secret
----------------------------

To include a secret value in an Idem SLS file without exposing the value, use the following example as a guideline.

Add code similar to the following to your SLS file. Vault versions v1 and v2 (the default) are supported. The example shows how to access the secret value stored under key name ``pwd`` at file path ``/credential``

.. code-block::

    my-secret:
    vault.secrets.kv_v2.secret.search:
      filepath: /credential
      pwd:
      test.succeed_with_comment:
      comment: ${vault.secrets.kv_v2.secret:my-secret:data:pwd}

By making an argument binding to ``my-secret``, the secret value can then be used within a resource in the SLS file.

Secret values can also be part of the idem state command ``--params`` option as shown in the following example.

.. code-block::

    idem state --param-sources "vault://vault_test@vault.example.com:8200" "file://path-to-file/params.sls" --params /path-to-token/token /path-to-file/my_state.sls

Where ``--param-sources`` is a list that can take different sources of format ``protocol://account_profile@url:port``

Create a new secret
-------------------

To create a new secret, add code similar to the following to your SLS file.

.. code-block::

    new-secret:
    vault.secrets.kv_v2.secret.present:
      path: /idem/test
      data: {"key1":"value1", "key2":"value2"}
