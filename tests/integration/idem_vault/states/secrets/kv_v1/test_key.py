import copy
import uuid

import pytest


@pytest.fixture(scope="module", autouse=True)
async def skip_kv_v1(version):
    if version != "v1":
        raise pytest.skip("Only runs on kv_v1")


@pytest.mark.asyncio
async def test_full(hub, ctx, version, resource_path):
    secret_name = f"idem-test-kv-v1-key-{uuid.uuid4()}"
    path = f"secret/idem-test-kv-v1-key-{uuid.uuid4()}"
    # create secrets with 2 keys in the above path
    data = {"my-secret": "my-secret-value", "new-secret": "new-secret-value"}

    ret = await hub.states.vault.secrets.kv_v1.secret.present(
        ctx, name=secret_name, data=data, **{resource_path: path}
    )
    assert ret["result"], ret["comment"]
    assert ret["changes"]
    assert ret["changes"]["new"]
    new_changes = ret["changes"]["new"]
    for change_key in new_changes:
        assert "*" == new_changes[change_key]
    # Add one more secret to the same path using key state
    key = "secret-key"
    value = "secret-value"
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.vault.secrets.kv_v1.key.present(
        test_ctx, name=secret_name, key=key, value=value, **{resource_path: path}
    )
    assert ret["result"], ret["comment"]
    assert f"Would create vault.secrets.kv_v1.key '{key}'." in ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    resource = ret["new_state"]
    assert key == resource.get("key")
    assert value == resource.get("value")
    assert ret["changes"]
    assert ret["changes"]["new"]
    new_changes = ret["changes"]["new"]
    for change_key in new_changes:
        assert "*" == new_changes[change_key]

    ret = await hub.states.vault.secrets.kv_v1.key.present(
        ctx, name=secret_name, key=key, value=value, **{resource_path: path}
    )
    assert ret["result"], ret["comment"]
    assert f"Created vault.secrets.kv_v1.key '{key}'." in ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    resource = ret["new_state"]
    assert key == resource.get("key")
    assert value == resource.get("value")
    assert ret["changes"]
    assert ret["changes"]["new"]
    new_changes = ret["changes"]["new"]
    for change_key in new_changes:
        assert "*" == new_changes[change_key]

    # Search using secret state should return newly added key along with other keys
    ret = await hub.exec.vault.secrets.kv_v1.secret.get(ctx, path=path)
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert 3 == len(resource.get("data"))

    # Update a key with new value with --test flag
    value = "update-secret-value"
    ret = await hub.states.vault.secrets.kv_v1.key.present(
        test_ctx, name=secret_name, key=key, value=value, **{resource_path: path}
    )
    assert ret["result"], ret["comment"]
    assert f"Would update vault.secrets.kv_v1.key '{key}'." in ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    resource = ret["new_state"]
    assert key == resource.get("key")
    assert value == resource.get("value")

    # Update the key
    ret = await hub.states.vault.secrets.kv_v1.key.present(
        ctx, name=secret_name, key=key, value=value, **{resource_path: path}
    )
    assert ret["result"], ret["comment"]
    assert f"Updated vault.secrets.kv_v1.key '{key}'." in ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    resource = ret["new_state"]
    assert key == resource.get("key")
    assert value == resource.get("value")
    assert ret["changes"]
    assert ret["changes"]["new"]
    new_changes = ret["changes"]["new"]
    for change_key in new_changes:
        assert "*" == new_changes[change_key]

    # Call key again with no change - idempotency
    ret = await hub.states.vault.secrets.kv_v1.key.present(
        ctx, name=secret_name, key=key, value=value, **{resource_path: path}
    )
    assert ret["result"], ret["comment"]
    assert f"nothing to be updated" in ret["comment"][0]
    assert ret["old_state"] and ret["new_state"]
    assert ret["old_state"] == ret["new_state"]

    # delete the key with --test
    ret = await hub.states.vault.secrets.kv_v1.key.absent(
        test_ctx, name=secret_name, path=path, key=key
    )
    assert ret["result"], ret["comment"]
    assert f"Would delete vault.secrets.kv_v1.key '{key}'." in ret["comment"]
    assert ret["old_state"] and not ret["new_state"]

    # Delete key in real
    ret = await hub.states.vault.secrets.kv_v1.key.absent(
        ctx, name=secret_name, path=path, key=key
    )
    assert ret["result"], ret["comment"]
    assert f"Deleted vault.secrets.kv_v1.key '{key}'." in ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert f"Deleted vault.secrets.kv_v1.key '{key}'." in ret["comment"]

    # Delete the same key again
    ret = await hub.states.vault.secrets.kv_v1.key.absent(
        ctx, name=secret_name, key=key, **{resource_path: path}
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and not ret["new_state"]
    assert f"vault.secrets.kv_v1.key '{key}' is already absent." in ret["comment"]

    # Search should now return only 2 keys
    ret = await hub.exec.vault.secrets.kv_v1.secret.get(ctx, path=path)
    assert ret["ret"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert 2 == len(resource.get("data"))

    # Destroy all keys in the path
    # Delete secret with all versions with test
    ret = await hub.states.vault.secrets.kv_v1.secret.absent(
        test_ctx, name=secret_name, **{resource_path: path}
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
