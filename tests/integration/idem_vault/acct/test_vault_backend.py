import pytest
from pytest_idem.runner import run_yaml_block

SECRET_PATH = "secret/profiles"


@pytest.mark.dependency(name="present")
def test_present(hub, ctx, esm_cache, acct_data, version: str):
    """
    Create a brand new secret for testing the vault acct backend
    """
    SECRET_PRESENT = f"""
    acct_secrets:
      vault.secrets.kv_{version}.secret.present:
        - path: {SECRET_PATH}
        - data:
            aws:
              aws_profile:
                aws_access_key_id: test_key
                aws_secret_access_key: test_secret
                region_name: test_region
            azure:
              azure_profile:
                client_id: test_client
                secret: test_secret
                subscription_id: test_sub
                tenant: test_tenant
            provider:
              profile:
                kw1: val1
                kw2: val2
    """

    # Run the state defined in the yaml block
    ret = run_yaml_block(SECRET_PRESENT, managed_state=esm_cache, acct_data=acct_data)
    resource_ret = next(iter(ret.values()))
    assert resource_ret["result"], "\n".join(resource_ret["comment"])


@pytest.mark.dependency(depends=["present"])
@pytest.mark.asyncio
async def test_unlock(hub, ctx):
    # patch the hub with acct providers if they aren't there
    for provider in ("aws", "azure", "provider"):
        hub.pop.sub.add(subname=provider, sub=hub.states)
        hub.states[provider].ACCT = [provider]

    # collect the pseudo profiles and verify their contents
    acct_data = {
        "profiles": {
            "acct-backends": {
                "vault": {"vault_backend_1": dict(path=SECRET_PATH, **ctx.acct)}
            }
        }
    }

    # AWS
    new_ctx = await hub.idem.acct.ctx(
        "states.aws.",
        profile="aws_profile",
        acct_data=acct_data,
    )
    assert new_ctx.acct == {
        "aws_access_key_id": "test_key",
        "aws_secret_access_key": "test_secret",
        "region_name": "test_region",
    }

    # Azure
    new_ctx = await hub.idem.acct.ctx(
        "states.azure.",
        profile="azure_profile",
        acct_data=acct_data,
    )
    assert new_ctx.acct == {
        "client_id": "test_client",
        "secret": "test_secret",
        "subscription_id": "test_sub",
        "tenant": "test_tenant",
    }

    # Provider
    new_ctx = await hub.idem.acct.ctx(
        "states.provider.",
        profile="profile",
        acct_data=acct_data,
    )
    assert new_ctx.acct == {
        "kw1": "val1",
        "kw2": "val2",
    }


@pytest.mark.dependency(depends=["present"])
def test_absent(esm_cache, acct_data, version: str):
    """
    Clean up the secret created for this test
    """
    SECRET_ABSENT = f"""
    acct_secrets:
      vault.secrets.kv_{version}.secret.absent:
        - path: {SECRET_PATH}
    """
    # Run the state defined in the yaml block
    ret = run_yaml_block(SECRET_ABSENT, managed_state=esm_cache, acct_data=acct_data)
    resource_ret = next(iter(ret.values()))
    assert resource_ret["result"], "\n".join(resource_ret["comment"])
    assert resource_ret["new_state"] is None
