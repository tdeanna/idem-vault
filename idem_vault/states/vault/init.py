def __init__(hub):
    # This enables acct profiles that begin with "vault" for states
    hub.states.vault.ACCT = ["vault"]
